#!/bin/bash

set -e

mkdir -p dist

DIST_PATH=$(pwd)/dist

# Install folly dependencies
sudo apt-get install -y \
	g++ \
	automake \
	autoconf \
	autoconf-archive \
	libtool \
	libboost-all-dev \
	libevent-dev \
	libdouble-conversion-dev \
	libgoogle-glog-dev \
	libgflags-dev \
	liblz4-dev \
	liblzma-dev \
	libsnappy-dev \
	make \
	zlib1g-dev \
	binutils-dev \
	libjemalloc-dev \
	libssl-dev

# Build and install folly (to ./dist/)
pushd folly/folly
autoreconf -ivf
./configure --prefix=$DIST_PATH
make -j$(getconf _NPROCESSORS_ONLN)
make install
popd

# Install wangle deps
sudo apt-get install cmake
pushd wangle/wangle
cmake . -DCMAKE_INSTALL_PREFIX:PATH=$DIST_PATH
make -j$(getconf _NPROCESSORS_ONLN)
make install
popd

# proxygen deps
sudo apt-get install google-mock

pushd proxygen/proxygen
autoreconf -ivf
CFLAGS="-I$DIST_PATH/include -L$DIST_PATH/lib" \
	CXXFLAGS="-I$DIST_PATH/include -L$DIST_PATH/lib" \
	LDFLAGS=-L$DIST_PATH/lib \
	./configure --prefix=$DIST_PATH 
make -j$(getconf _NPROCESSORS_ONLN)
make prefix=$DIST_PATH install
popd

echo "Done"
