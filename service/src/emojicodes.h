//
//

#include <iostream>
#include <string>
#include <iterator>

#include <boost/iostreams/pipeline.hpp>
#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/operations.hpp>
#include <boost/container/flat_map.hpp> 

#include <codecvt>

#include "json.hpp"

#pragma once

namespace {
static constexpr auto kEmojiJsonPath = "/home/yan/src/emoji-data/emoji.json";
}

// shortcode filter maintains state, so it needs to be flushable
struct multichar_flushable : boost::iostreams::multichar_output_filter_tag,
                             boost::iostreams::flushable_tag { };

// A boost stream filter that replaces emoji shortnames (i.e. :+1:) with their utf8-encoded
// equivalents. Built on a vastly-simplified Aho-Corasick string matching algorithm.
class emoji_shortcode_filter {
public:
    typedef char                char_type;
    typedef multichar_flushable category;
    typedef char32_t            codepoint_t;

    // Initialize filter with an emoji json blob.
    // i.e. https://github.com/iamcal/emoji-data/blob/master/emoji.json
    explicit emoji_shortcode_filter(const nlohmann::json &init)
    {
        initialize(init);
    }

    emoji_shortcode_filter()
    {
        std::ifstream fs{kEmojiJsonPath};
        auto emojis = nlohmann::json::parse(fs);

        initialize(emojis);
    }

    // XXX(yan): filtering_ostream makes a copy of the filter. Move root to a 
    // shared_ptr and use COW.
    emoji_shortcode_filter(const emoji_shortcode_filter &rhs)
    : root_{rhs.root_}
    , current_{rhs.current_}
    , rendered_{rhs.rendered_}
    {
        stage_.reserve(kStageSize);
    }

    // @brief Filter bytes through to |sink|, replacing all emoji shortnames
    // (such as :+1:) by unicode equivalents.
    template <typename Sink>
    std::streamsize write(Sink &sink, const char *s, std::streamsize n)
    {
        std::streamsize rest = n;
	while (rest != 0) {
            unsigned char c = *s;

            if (c == ':') {
                // If current_ still points to root, we have consecutive colons
                // In this case, just reset the state. If current_ is not null
                // or root, we've just finished matching.
                if (current_ == nullptr || current_ == &root_) {
                    current_ = &root_;
                } else {
                    // We did not match. Treat this as the start of the next 
                    // emoji in case we see a pattern like :fail:success:
                    if (current_->to == 0) {
                        boost::iostreams::put(sink, c);
                        boost::iostreams::write(sink, stage_.data(), stage_.size());
                        current_ = &root_;
                    } else {
                        // Print the actual emoji here
                        size_t length = current_->to - current_->from;
                        boost::iostreams::write(sink, &rendered_[current_->from], length);

                        current_ = nullptr;
                    }
                }
                stage_.clear();
            } else {

                // We're currently trying to match. If we didn't current_ will be set
                // to nullptr, which is okay, this essentially abandons the current_
                // match state and resets it for the next time we see a colon
                if (current_ != nullptr) {
                    stage_.push_back(c);
                    current_ = current_->getLink(c);

                    if (current_ == nullptr) {
                        boost::iostreams::put(sink, ':');
                        boost::iostreams::write(sink, stage_.data(), stage_.size());
                        stage_.clear();
                    }
                } else {
                    // We're not matching, just forward the character
                    boost::iostreams::put(sink, c);
                }
            }

	    --rest;
            s++;
        }
	return n - rest;
    }

    template<typename Device>
    bool flush(Device& sink)
    {
        // If the buffer ended in the middle of trying to match a short-code,
        // send what we buffered so far and reset the matching fsm.
        auto n = stage_.size();
        if (n != boost::iostreams::write(sink, stage_.data(), n))
            return false;

        stage_.clear();

        if (current_ != nullptr) {
            if (!boost::iostreams::put(sink, ':'))
                return false;
        }

        current_ = nullptr;

        return true;
    }

private:

    void initialize(const nlohmann::json &init)
    {
        std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> converter_;

        for (auto &emoji : init) {

            std::string shortname = emoji["short_name"];
            std::string unicode = emoji["unified"];

            std::stringstream cpstream(unicode);
            std::string cp;
            std::vector<codepoint_t> codepoints;
            while (std::getline(cpstream, cp, '-')) {
                codepoint_t point = std::stoul(cp, nullptr, 16);
                codepoints.push_back(point);
            }

            auto bytes = converter_.to_bytes(&*std::begin(codepoints), &*std::end(codepoints));

            auto from = rendered_.size();
            std::copy(std::begin(bytes), std::end(bytes), std::back_inserter(rendered_));
            auto to = rendered_.size();

            insert(shortname, from, to);
        }
    }
    constexpr const static size_t kStageSize = 64;

    constexpr const static size_t kNumCodepoints = 4;

    class node
    {
    public:
        boost::container::flat_map<char, node> links;
        size_t from = 0;
        size_t to = 0;

        node* getLink(char c)
        {
            auto iter = links.find(c);
            if (iter != links.end()) {
                return &iter->second;
            } else {
                return nullptr;
            }
        }
    };

    void insert(const std::string &pattern, size_t from, size_t to)
    {
        node *n = &root_, *child;

        for (char c : pattern) {
            child = n->getLink(c);
            if (child == nullptr) {
                child = &n->links[c];
            }
            n = child;
        }
        n->from = from;
        n->to = to;
    }

    size_t total_nodes(const node *start, int depth = 1) const
    {
        static size_t max_depth;
        if (depth > max_depth) {
            max_depth = depth;
        }
        size_t total = 1;
        for (const auto &itm : start->links) {
            total += total_nodes(&itm.second, depth + 1);
        }
        return total;
    }

    node root_;

    node *current_ = nullptr;

    std::vector<char> stage_;

    std::vector<char> rendered_;
};

BOOST_IOSTREAMS_PIPABLE(emoji_shortcode_filter, 0)

