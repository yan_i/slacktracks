
#include <chrono>
#include <sstream>
#include <iostream>
#include <map>

#include <cstdlib>

#include <folly/futures/Future.h>
#include <folly/Memory.h>
#include <folly/Portability.h>
#include <folly/io/async/EventBaseManager.h>
#include <gflags/gflags.h>
#include <glog/logging.h>
#include <proxygen/httpserver/HTTPServer.h>
#include <proxygen/httpserver/RequestHandlerFactory.h>
#include <proxygen/httpserver/ResponseBuilder.h>
#include <unistd.h>

#include <json.hpp>


#include "slackapi.hpp"
#include "oauthhandler.h"
#include "db.h"

#pragma once

namespace slackapi
{
using namespace proxygen;
using json = nlohmann::json;


struct Stats {
    int requests;
};


class SlackAPIHandler : public RequestHandler
{
    inline void
    setBodyToJson(ResponseBuilder &response, const json &obj) const;

public:
    explicit SlackAPIHandler(slackapi::slack *api)
        :api_{api} { }

    void onRequest(std::unique_ptr<HTTPMessage> msg) noexcept override;

    void onBody(std::unique_ptr<folly::IOBuf> body) noexcept override;

    void handleMessageEvent(const json &obj) const;

    void onEOM() noexcept override;

    void onUpgrade(UpgradeProtocol proto) noexcept override;

    void requestComplete() noexcept override;

    void onError(ProxygenError err) noexcept override;

private:
    slackapi::slack *api_;

    std::unique_ptr<folly::IOBuf> body_;
};

// Unconditionally redirect to a new URL
class RedirectHandler : public RequestHandler {
public:
    explicit RedirectHandler(const std::string &url):url_{url} { }

    void onEOM() noexcept override {
        ResponseBuilder(downstream_)
                .status(302, "Redirect")
                .header("Location", url_)
                .sendWithEOM();
    }

    void onRequest(std::unique_ptr<HTTPMessage>) noexcept override { }
    void onBody(std::unique_ptr<folly::IOBuf> body) noexcept override { }
    void onUpgrade(proxygen::UpgradeProtocol prot) noexcept override { }
    void requestComplete() noexcept override { delete this; }
    void onError(ProxygenError err) noexcept override { delete this; }
private:
    std::string url_;
};

class NotFoundHandler : public RequestHandler {
public:
    explicit NotFoundHandler() { }

    void onEOM() noexcept override {
        ResponseBuilder(downstream_)
                .status(404, "Not Found")
                .sendWithEOM();
    }

    void onRequest(std::unique_ptr<HTTPMessage>) noexcept override { }
    void onBody(std::unique_ptr<folly::IOBuf> body) noexcept override { }
    void onUpgrade(proxygen::UpgradeProtocol prot) noexcept override { }
    void requestComplete() noexcept override { delete this; }
    void onError(ProxygenError err) noexcept override { delete this; }
};


class SlackAPIHandlerFactory : public RequestHandlerFactory,
                               public slack_visitor
{
public:
    virtual void onMessage(std::shared_ptr<events::Message> m) override {
        auto &db = slacktracks_db::getInstance();
        db.storeMessage(m).then([]() {
                });
    }

    virtual void onReactionAdded(std::shared_ptr<events::ReactionAdded>)  override {
        LOG(INFO) << "Got reaction added";
    }

    virtual void onReactionRemoved(std::shared_ptr<events::ReactionRemoved>) override  {
        LOG(INFO) << "Got reaction removed";
    }

    void
    onServerStart(folly::EventBase *evb) noexcept override
    {
        //auto ptr = new Stats;
        //LOG(INFO) << "Got stats: " << ptr;
        //stats_.reset(ptr);
        api_.reset(new slackapi::slack(std::getenv("SLACKTRACKS_TOKEN")));

        evb_ = evb;

        api_->addVisitor(this);
    }

    void
    handleEventCallback(slackapi::events::Message m) const
    {
    }
    void
    onServerStop() noexcept override
    {
    }

    RequestHandler *
    onRequest(RequestHandler *handler, HTTPMessage *msg) noexcept override
    {
        assert(msg != nullptr);

        const std::string &path = msg->getPath();

        if (path == "/") {
            auto target = api_->createOauthRequestURL();// generateOauthAuthorizeRequest();
            return new RedirectHandler(target);
        //} else if (path == "/list") {

        } else if (path == "/api/messages") {
            return new SlackAPIHandler(api_.get());
        } else if (path == "/oauth") {
            return new OAuthHandler{evb_, api_.get()};
        } else {
            LOG(ERROR) << "Unknown request path: " << msg->getURL();
            // XXX(yan): return something else
            return new RedirectHandler(root_);
        }
    }

private:

    static constexpr auto root_ = "https://st.srtd.org/";

    folly::ThreadLocalPtr<slackapi::slack> api_;
    folly::EventBase *evb_;

};
}
