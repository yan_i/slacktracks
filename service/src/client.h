//
// Created by user on 11/24/16.
//

#ifndef SLACKTRACKS_SVC_CLIENT_H
#define SLACKTRACKS_SVC_CLIENT_H

// #include <HTTPConnector.h>
#include <memory>
#include <string>
#include <folly/io/async/SSLContext.h>
#include <proxygen/lib/http/HTTPConnector.h>
#include <proxygen/lib/http/session/HTTPTransaction.h>
#include <proxygen/lib/utils/URL.h>
#include <folly/io/async/EventBase.h>
#include <folly/futures/Promise.h>

#include <json.hpp>


#pragma once

namespace slackapi {

namespace p = proxygen;
namespace f = folly;
namespace j = nlohmann;

class HttpClient : public p::HTTPConnector::Callback,
                   public p::HTTPTransactionHandler {
public:
    HttpClient(f::EventBase *evb, const std::string &url, std::shared_ptr<f::Promise<std::string>> promise)
        : evb_{evb}
        , url_{url}
        , promise_{promise}
    {
    }
    ~HttpClient() override;

    void initSsl(const std::string& certPath,
                 const std::string& nextProtos);

    void sslHandshakeFollowup(p::HTTPUpstreamSession* session) noexcept;

    // Connector callbacks
    void connectSuccess(p::HTTPUpstreamSession* session) override;
    void connectError(const f::AsyncSocketException& ex) override;

    // Transaction Handler callbacks
    void onTrailers(std::unique_ptr<p::HTTPHeaders>) noexcept override {
        LOG(INFO) << "Discarding trailers";
    }
    void onEOM() noexcept override {
        if (body_) {
            std::stringstream ss;
            const f::IOBuf* p = body_.get();
            do {
                std::string foo((const char*)p->data(), p->length());

                ss.write((const char*)p->data(), p->length());
                p = p->next();
            } while (p != body_.get());

            promise_->setValue(ss.str());
        } else {
            promise_->setValue("");
        }
    }
    void onUpgrade(p::UpgradeProtocol) noexcept override {
    }
    void onError(const p::HTTPException& error) noexcept override {
        promise_->setException(error);
    }
    void onEgressPaused() noexcept override { }
    void onEgressResumed() noexcept override { }
    void setTransaction(p::HTTPTransaction*) noexcept override { }

    void detachTransaction() noexcept override {
        delete this;
    }

    void onHeadersComplete(std::unique_ptr<p::HTTPMessage> msg) noexcept override {
    }

    void onBody(std::unique_ptr<f::IOBuf> chain) noexcept override {
        if (body_) {
          body_->prependChain(std::move(chain));
        } else {
          body_ = std::move(chain);
        }
    }


    static std::string get(const std::string &url);
    folly::SSLContextPtr getSSLContext() { return ssl_context_; }

private:
    f::EventBase *evb_;
    std::shared_ptr<f::Promise<std::string>> promise_;
    p::URL url_;
    std::shared_ptr<f::SSLContext> ssl_context_;
    std::unique_ptr<folly::IOBuf> body_;

};


}
#endif //SLACKTRACKS_SVC_CLIENT_H
