#include <string>
#include <utility>
#include <sstream>
#include <algorithm>
#include <folly/futures/Promise.h>

#include <json.hpp>

#include "slackapi.hpp"
#include "clientthread.h"
#include "types.h"


namespace slackapi {

using nlohmann::json;

constexpr const char *slack::scopes_[];
constexpr const char* const slack::client_id_;
constexpr const char* const slack::client_secret_;

void
slack::receivedJson(const json &from)
{
    if (from["token"] != token_) {
        LOG(ERROR) << "Bad token: " << from["token"];
        return;
    }

    if (from["type"] != "event_callback") {
        LOG(ERROR) << "Bad callback type: " << from["type"];
        return;
    }

    auto &event = from["event"];

    if (event["type"] == "message") {
        VLOG(INFO) << "Got message";

        auto msg = std::make_shared<events::Message>(event);
        for (auto &visitor : visitors_) {
            visitor->onMessage(msg);
        }
    } else if (event["type"] == "reaction_added") {
        VLOG(INFO) << "Got reaction_added";

        auto ra = std::make_shared<events::ReactionAdded>(event);
        for (auto &visitor : visitors_) {
            visitor->onReactionAdded(ra);
        }
    } else if (event["type"] == "reaction_removed") {
        VLOG(INFO) << "Got reaction_removed";

        auto rr = std::make_shared<events::ReactionRemoved>(event);
        for (auto &visitor : visitors_) {
            visitor->onReactionRemoved(rr);
        }

    } else {
        LOG(ERROR) << "Bad message type: " << event["type"];
    }
}
folly::Future<json>
slackRequest(const std::string &method, const std::map<std::string, std::string> args) {
    constexpr static char base[] {"https://slack.com/api/"};

    std::stringstream url;
    url << base << method << "?";
    for (auto &arg : args) {
        url << arg.first << "=" << arg.second << "&";
    }
    
    auto response_s = httpRequestSimple(url.str());
    
    return response_s.then([](std::string response) {
        return json::parse<std::string>(response);
    });
}

folly::Future<std::vector<channel>>
slack::listChannels(const team &t)
{

    auto channels_future = slackRequest("channels.list", {{"token", t.access_token}})
        .then([](json jchannels) {
            std::vector<channel> channels;

            // TODO(yan): error checking here

            for (auto &jchannel : jchannels["channels"]) {
                channels.emplace_back(jchannel);
            }
            return std::move(channels);
        });

    return channels_future;
}

namespace {


// Maximum number of messages to grab per request
constexpr unsigned kMessagesPerRequest = 200;

// Helper to asynchronously receive multiple "pages" of messages worth of a 
// channel's history
void
getChannelHistoryInner(const team &t, std::string channel_id, unsigned int count,
                         std::chrono::system_clock::time_point from,
                         std::chrono::system_clock::time_point to,
                         std::vector<Message> &&msgs,
                         folly::Promise<std::vector<Message>> &&promise)
{
    using std::move;

    ClientRequestThread::run([=, msgs=move(msgs), p=move(promise)]() mutable {
            auto from_s = timestampToString(from);
            auto to_s = timestampToString(to);

            auto fut = slackRequest("channels.history",
                                        {{"token",   t.access_token},
                                         {"channel", channel_id},
                                         {"latest",  to_s},
                                         {"oldest",  from_s},
                                         {"inclusive", "0"},
                                         {"count", std::to_string(kMessagesPerRequest)}});

            fut.then([=, msgs=move(msgs), p=move(p)](json r) mutable {

                auto earliest = std::chrono::system_clock::time_point::max();

                VLOG(2) << "Received " << r["messages"].size() << " messages.";

                for (auto m : r["messages"]) {
                    msgs.emplace_back(m);
                    earliest = std::min(msgs.back().timestamp(), earliest);
                }

                if (r["has_more"].get<bool>()) {
                    getChannelHistoryInner(t, channel_id, count, from, earliest,
                            move(msgs), move(p));
                } else {
                    p.setValue(move(msgs));
                }
            });
    });

}
}

folly::Future<std::vector<Message>>
slack::getChannelHistory(const team &t, std::string channel_id, unsigned int count,
                         std::chrono::system_clock::time_point from,
                         std::chrono::system_clock::time_point to)
{

    std::vector<Message> msgs;
    folly::Promise<decltype(msgs)> promise;

    auto f = promise.getFuture();
    getChannelHistoryInner(t, channel_id, count, from, to,
                             std::move(msgs),
                             std::move(promise));
    return f;
}

std::string
slack::createOauthRequestURL() const {
    std::stringstream url;

    url << "https://slack.com/oauth/authorize?"
        << "client_id=" << client_id_ << "&"
        << "scope=";

    std::ostream_iterator<const char*> out_it (url, " ");
    std::copy(std::begin(scopes_), std::end(scopes_), out_it );

    url << "&redirect_uri=" << root_ << "oauth";

    return url.str();
}   

folly::Future<team>
slack::authenticateTeam(const std::string &code)
{
    auto oauth_future =
        slackRequest("oauth.access", {
            {"client_id", client_id_},
            {"client_secret", client_secret_},
            {"code", code},
            {"redirect_uri", std::string(root_) + "oauth"}})
        .then([](json resp) {
                if (!resp["ok"].get<bool>()) {
                    throw std::runtime_error{"Could not get oauth token"};
                }

                team t{resp};

                VLOG(1) << "Added new team " << t.name;

                return std::move(t);
            });

    return oauth_future;
}

}

