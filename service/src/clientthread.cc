
#include <proxygen/lib/http/HTTPConnector.h>
#include <proxygen/lib/http/session/HTTPTransaction.h>
#include <proxygen/lib/utils/URL.h>

#include "clientthread.h"
#include "client.h"

namespace slackapi {

folly::Future<std::string>
httpRequestSimple(std::string url_s)
{
    auto p = std::make_shared<folly::Promise<std::string>>();
    auto fut = p->getFuture();

    auto &crt = ClientRequestThread::getInstance();
    crt.evb_.runInEventBaseThread(
        [e=&crt.evb_, t=crt.timer_.get(),  url_s=std::move(url_s), p]() mutable {
            auto client = new HttpClient{e, url_s, std::move(p)};
            auto connector = new proxygen::HTTPConnector{client, t};

            bool https = url_s.compare(0, 5, "https") == 0;

            if (https) {
                client->initSsl("/etc/ssl/certs/ca-certificates.crt",
                                /*"h2,h2-14,*/"spdy/3.1,spdy/3,http/1.1");
            }

            const folly::AsyncSocket::OptionMap opts{{{SOL_SOCKET, SO_REUSEADDR}, 1}};

            proxygen::URL url{url_s};
            folly::SocketAddress addr(url.getHost(), url.getPort(), true);
            if (https) {
                connector->connectSSL(
                        e, addr, client->getSSLContext(), nullptr,
                        std::chrono::seconds(10), opts,
                        folly::AsyncSocket::anyAddress(), url.getHost());
            } else {
                connector->connect(e, addr, std::chrono::seconds(10), opts);
            }
    });

    return fut;
}

}
