#include <vector>
#include <sstream>

#include <variant>

#include <json.hpp>
#include "utils.h"

#pragma once

namespace slackapi {

using json = nlohmann::json;
using namespace google;

using user_t = std::string;

namespace events {

class ReactionBase {
public:
    ReactionBase(const json &from)
    : reaction_{from["reaction"].get<decltype(reaction_)>()}
    , item_user_ {from["item_user"].get<decltype(item_user_)>()}
    , user_{from["user"].get<decltype(user_)>()}
    {
    }

protected:
    std::string reaction_, user_, item_user_;
};

class ReactionAdded : public ReactionBase {
public:
    ReactionAdded(const json &from)
    : ReactionBase{from} {
        assert(from["type"] == "reaction_added");

    }
};

class ReactionRemoved : public ReactionBase {
public:

    ReactionRemoved(const json &from)
    : ReactionBase{from}
    {
        assert(from["type"] == "reaction_removed");
    }
};

class Message {
public:
    enum class eSubtype {
        kBotMessage, // A message was posted by an integration
        kChannelArchive, // A channel was archived
        kChannelJoin, // A team member joined a channel
        kChannelLeave, // A team member left a channel
        kChannelName, // A channel was renamed
        kChannelPurpose, // A channel purpose was updated
        kChannelTopic, // A channel topic was updated
        kChannelUnarchive, // A channel was unarchived
        kFileComment, // A comment was added to a file
        kFileMention, // A file was mentioned in a channel
        kFileShare, // A file was shared into a channel
        kGroupArchive, // A group was archived
        kGroupJoin, // A team member joined a group
        kGroupLeave, // A team member left a group
        kGroupName, // A group was renamed
        kGroupPurpose, // A group purpose was updated
        kGroupTopic, // A group topic was updated
        kGroupUnarchive, // A group was unarchived
        kMeMessage, // A /me message was sent
        kMessageChanged, // A message was changed
        kMessageDeleted, // A message was deleted
        kPinnedItem, // An item was pinned in a channel
        kUnpinnedItem, // An item was unpinned from a channel
    };

    // A message_deleted subtype
    class MessageDeleted {
    public:
        MessageDeleted (const MessageDeleted &from)
            : deleted_ts_{from.deleted_ts_}
        {

        }


        MessageDeleted(const json &from)
            : MessageDeleted{parseTime(from["deleted_ts"])}
        {

        }

        MessageDeleted(std::chrono::system_clock::time_point deleted_ts)
            : deleted_ts_{deleted_ts} { }
    private:
        std::chrono::system_clock::time_point deleted_ts_;
    };

    class MessageChanged {
    public:
        MessageChanged(const json &from)
        : previous_message_{std::make_unique<Message>(from)}
        {
        }

        MessageChanged() = default;
        MessageChanged (const MessageChanged &rhs)
        : previous_message_{std::make_unique<Message>(*rhs.previous_message_)}
        {
        }

    private:
        const std::unique_ptr<Message> previous_message_;
    };

    class ChannelTopic {
    public:
        ChannelTopic(const std::string &topic)
        :topic_{topic}
        {
        }

        ChannelTopic() = default;

        ChannelTopic(const ChannelTopic &rhs)
        : topic_{rhs.topic_}
        {
        }
    private:
        std::string topic_;
    };

    // A bot_message subtype
    class BotMessage {
    public:
    };

#define FIELD(x) x{from[#x].get<decltype(x)>()}
    struct EmbeddedReaction {
        EmbeddedReaction(const json &from)
        : FIELD(name)
        , FIELD(count)
        {
            for (auto user : from["users"]) {
                users.push_back(user);
            }
        }

        std::string name;
        uint32_t count;
        std::vector<std::string> users;
    };

    struct Attachment {
        Attachment(const json &from)
        : FIELD(fallback)
        , FIELD(title)
        , FIELD(title_link)
        {
        }

        std::string fallback, title, title_link;
        uint32_t color;
    };

    Message() = default;


    explicit Message(const json &from);

    explicit Message(std::chrono::system_clock::time_point ts)
        : ts_{ts}
    {
#if 0
        static_assert(std::is_copy_constructible<BotMessage>::value, "bot");
        static_assert(std::is_copy_constructible<MessageChanged>::value, "change");
        static_assert(std::is_copy_constructible<MessageDeleted>::value, "deleted");
#endif
    }

    ~Message() = default;

    void setUser(const std::string &user) {
        user_ = user;
    }

    std::string user(void) const {
        return user_;
    }

    void setChannel(const std::string &channel) {
        channel_ = channel;
    }

    std::string channel(void) const {
        return channel_;
    }

    void setContents(const std::string &text) {
        text_ = text;
    }

    std::string text(void) const { return text_; }

    bool hidden(void) const {
        return hidden_;
    }

    Message(const Message &m)
    : text_{m.text_}
    , ts_{m.ts_}
    , channel_{m.channel_}
    , subtype_{m.subtype_}
    , hidden_{m.hidden_}
    {
    }

    std::string timestamp_str(void) const {
        return timestampToString(ts_);
    }

    std::string str(void) const {
        std::stringstream ss;
        ss << "msg(ts:" << timestamp_str() << ", txt: '" << text_ << "'";
        if (!reactions_.empty()) {
            ss << " [reactions ";
            for (auto &reaction : reactions_) {
                ss << reaction.name << ":" << reaction.count << " ";
            }
            ss << "]";
        }

        if (!attachments_.empty()) {
            ss << "[attachments ";
            for (auto &att : attachments_) {
                ss << att.title << " ";
            }
            ss << "]";
        }
        ss << ")";

        return ss.str();
    }

    const auto &attachments(void) const {
        return attachments_;
    }

    const auto &reactions(void) const {
        return reactions_;
    }

    const std::chrono::system_clock::time_point timestamp(void) const {
        return ts_;
    }

private:
    void setSubtype(const json &from);

    std::chrono::system_clock::time_point ts_;

    std::string channel_;

    user_t user_;

    std::experimental::variant<std::experimental::monostate,
        BotMessage,
        ChannelTopic,
        MessageChanged,
        MessageDeleted
    > subtype_;

    std::vector<EmbeddedReaction> reactions_;
    std::vector<Attachment> attachments_;

    bool hidden_;

    std::string text_;

};

}

}
