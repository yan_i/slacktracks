
#include <cstdlib>

#include <json.hpp>
#include <glog/logging.h>

#include "slackevent.hpp"
#include "clientthread.h"
#include "types.h"
#include "db.h"

#pragma once

namespace slackapi {

using nlohmann::json;
using namespace google;

struct APIConfig {
    static const APIConfig &Running(void) {
        return _self;
    }

    APIConfig() {
        const char *tok = std::getenv("SLACKTRACKS_TOKEN");
        if (tok == nullptr) {
            throw std::invalid_argument("SLACKTRACKS_TOKEN is not provided");
        }

        Token = std::string(tok);
    }

    std::string Token;
private:
    static APIConfig _self;
};

//
// Not exactly a visitor, but 
//
//
class slack_visitor {
public:
    virtual void onMessage(std::shared_ptr<events::Message>) = 0;
    virtual void onReactionAdded(std::shared_ptr<events::ReactionAdded>) = 0;
    virtual void onReactionRemoved(std::shared_ptr<events::ReactionRemoved>) = 0;
};

//
//
//
//
class slack {
    typedef std::chrono::system_clock system_clock;

public:
    slack(std::string token) :token_{token} { }

    void receivedJson(const json &from);

    void addVisitor(slack_visitor *visitor) {
        visitors_.push_back(visitor);
    }


    // Retrieve a channel's message history
    folly::Future<std::vector<Message>>
    getChannelHistory(const team &t, std::string channel_id,
        unsigned int count = 0,
        system_clock::time_point from = system_clock::time_point{std::chrono::seconds{0}},
        system_clock::time_point to = system_clock::now());

    // Retrieve a list of channels on a given team
    folly::Future<std::vector<channel>> listChannels(const team &t);

    // Convert a code received from oauth to an authenticated team
    folly::Future<team> authenticateTeam(const std::string &code);

    static constexpr auto root_ = "https://st.srtd.org/";

    std::string createOauthRequestURL() const;  


private:
    static constexpr auto client_id_ = "11499394807.100886116338";

    static constexpr auto client_secret_ = "e04c7510d3ed04dcc82cb09ded3a2fa2";

    static constexpr const char *scopes_[] = {
	    "channels:history",
	    "channels:read",
	    "emoji:read",
	    "groups:history",
	    "groups:read",
	    "reactions:read",
	    "users:read"
    };

    std::string token_;

    std::vector<slack_visitor*> visitors_;
};

folly::Future<json>
slackRequest(const std::string &method, const std::map<std::string, std::string> args);

}
