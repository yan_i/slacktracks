#include <glog/logging.h>
#include <cassert>
#include <variant>

#include "slackevent.hpp"
#include "slackapi.hpp"

namespace slackapi {

namespace events {

Message::Message(const json &from)
:ts_{parseTime(from["ts"])}
{
    assert(from["type"] == "message");

    if (from.find("subtype") != from.end()) {
        setSubtype(from);
    }

    if (from.find("hidden") != from.end()) {
        hidden_ = from["hidden"];
    }

    auto channel = from.find("channel");
    if (channel != from.end()) {
        channel_ = *channel;
    }

    auto user = from.find("user");
    if (user != from.end()) {
        user_ = *user;
    }

    auto text = from.find("text");
    if (text != from.end()) {
        text_ = *text;
    }

    auto attachments = from.find("attachments");
    if (attachments != from.end()) {
        for (auto &attachment : *attachments) {
            attachments_.emplace_back(attachment);
        }
    }
                    
    auto reactions = from.find("reactions");
    if (reactions != from.end()) {
        for (auto &reaction : *reactions) {
            reactions_.emplace_back(reaction);
        }
    }

}

void
Message::setSubtype(const json &from)
{
    auto subtype_str = from["subtype"];

/*
    if (subtype_str == "bot_message") {
        return Message::eSubtype::kBotMessage;
    } else if (subtype_str == "channel_archive") {
        return Message::eSubtype::kChannelArchive;
    } else if (subtype_str == "channel_join") {
        return Message::eSubtype::kChannelJoin;
    } else if (subtype_str == "channel_leave") {
        return Message::eSubtype::kChannelLeave;
    } else if (subtype_str == "channel_name") {
        return Message::eSubtype::kChannelName;
    } else if (subtype_str == "channel_purpose") {
        return Message::eSubtype::kChannelPurpose;
    } else */ if (subtype_str == "channel_topic") {
        subtype_.emplace<ChannelTopic>(from["topic"].get<std::string>());
    }  else /*if (subtype_str == "channel_unarchive") {
        return Message::eSubtype::kChannelUnarchive;
    } else if (subtype_str == "file_comment") {
        return Message::eSubtype::kFileComment;
    } else if (subtype_str == "file_mention") {
        return Message::eSubtype::kFileMention;
    } else if (subtype_str == "file_share") {
        return Message::eSubtype::kFileShare;
    } else if (subtype_str == "group_archive") {
        return Message::eSubtype::kGroupArchive;
    } else if (subtype_str == "group_join") {
        return Message::eSubtype::kGroupJoin;
    } else if (subtype_str == "group_leave") {
        return Message::eSubtype::kGroupLeave;
    } else if (subtype_str == "group_name") {
        return Message::eSubtype::kGroupName;
    } else if (subtype_str == "group_purpose") {
        return Message::eSubtype::kGroupPurpose;
    } else if (subtype_str == "group_topic") {
        return Message::eSubtype::kGroupTopic;
    } else if (subtype_str == "group_unarchive") {
        return Message::eSubtype::kGroupUnarchive;
    } else if (subtype_str == "me_message") {
        return Message::eSubtype::kMeMessage;
    } else */ if (subtype_str == "message_changed") {
        subtype_.emplace<MessageChanged>(from["message"]);
        // std::experimental::get<MessageChanged>(subtype_).initFromJson(from);
    } else if (subtype_str == "message_deleted") {
        subtype_.emplace<MessageDeleted>(parseTime(from["deleted_ts"]));
    /* }  else if (subtype_str == "pinned_item") {
        return Message::eSubtype::kPinnedItem;
    } else if (subtype_str == "unpinned_item") {
        return Message::eSubtype::kUnpinnedItem;
        */
    } else {
        LOG(ERROR) << "Unsupported subtype: " << subtype_str << "\n\n" << from.dump(2);
        // throw std::runtime_error("Bad message subtype name");
    }
}

}
}
