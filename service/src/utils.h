#include <chrono>

#pragma once

namespace slackapi {

std::chrono::system_clock::time_point parseTime(const std::string timestamp_s);

std::string
timestampToString(const std::chrono::system_clock::time_point &tp);
}
