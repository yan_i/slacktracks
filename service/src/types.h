

#include <string>
#include <chrono>
#include <cstdint>

#include <json.hpp>

#pragma once

// Light wrappers around slack api types
// https://api.slack.com/types


namespace slackapi {
struct channel {
    channel(const nlohmann::json j)
    : id{j["id"].get<decltype(id)>()}
    , name{j["name"].get<decltype(name)>()}
    , topic{j["topic"]["value"].get<decltype(topic)>()}
    , created{j["created"].get<decltype(created)>()}
    , nmembers{j["num_members"].get<decltype(nmembers)>()}
    {}

    channel(const std::string &id,
            const std::string &name,
            const std::string &topic,
            uint64_t created)
    : id{id}
    , name{name}
    , topic{topic}
    , created{created}
    {
    }

    channel(channel&&) = default;
    channel(const channel&) = default;
    channel &operator=(const channel&) = default;

    std::string id, name, topic;
    uint64_t created, nmembers;
};

struct team {
    team(const nlohmann::json &j)
    : id{j["team_id"].get<decltype(id)>()}
    , name{j["team_name"].get<decltype(name)>()}
    , user{j["user_id"].get<decltype(user)>()}
    , scope{j["scope"].get<decltype(scope)>()}
    , access_token{j["access_token"].get<decltype(access_token)>()}
    {
    }

    team(const std::string &id,
         const std::string &name,
         const std::string &user,
         const std::string &scope,
         const std::string &access_token)
    : id{id}
    , name{name}
    , user{user}
    , scope{scope}
    , access_token{access_token}
    {}

    team(team&&) = default;
    team(const team&) = default;
    team &operator=(const team &) = default;

    std::string id, name, user, scope, access_token;
};


}
