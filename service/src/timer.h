
#include <glog/logging.h>
#include <chrono>

#pragma once

class Timer {
public:
    Timer(std::string msg)
    : start_time_{std::chrono::high_resolution_clock::now()}
    , msg_{msg}
    { }

    ~Timer()
    {
        auto delta = std::chrono::high_resolution_clock::now() - start_time_;
        auto ms = std::chrono::duration_cast<std::chrono::microseconds>(delta);
        VLOG(1) << msg_ << " : " << ms.count() << "µs";
    }

private:
    std::chrono::system_clock::time_point start_time_;
    std::string msg_;

};


