/* */

#include <chrono>
#include <sstream>
#include <iostream>

#include <folly/Memory.h>

#include <folly/Portability.h>
#include <folly/io/async/EventBaseManager.h>
#include <gflags/gflags.h>
#include <glog/logging.h>
#include <proxygen/httpserver/HTTPServer.h>
#include <proxygen/httpserver/RequestHandlerFactory.h>
#include <proxygen/httpserver/ResponseBuilder.h>
#include <unistd.h>

#include "slackhandler.hpp"
#include "slackapi.hpp"
#include "slackevent.hpp"
#include "clientthread.h"
#include "db.h"

using slackapi::APIConfig;
APIConfig APIConfig::_self;

DEFINE_string(cert, "/etc/letsencrypt/live/st.srtd.org/cert.pem",
                    "Path to TLS certificate");
DEFINE_string(key, "/etc/letsencrypt/live/st.srtd.org/privkey.pem",
                    "Path to TLS certificate");

DEFINE_int32(https_port, 443, "Port to listen on with HTTP protocol");
DEFINE_string(ip, "localhost", "IP/Hostname to bind to");
DEFINE_int32(threads,
             0,
             "Number of threads to listen on. Numbers <= 0 "
             "will use the number of cores on this machine.");


int
main(int argc, char *argv[])
{
    using namespace proxygen;
    using namespace folly;
    using namespace std::chrono_literals;

    gflags::ParseCommandLineFlags(&argc, &argv, true);
    google::InitGoogleLogging(argv[0]);
    google::InstallFailureSignalHandler();

    std::vector<HTTPServer::IPConfig> IPs = {
        {SocketAddress(FLAGS_ip, FLAGS_https_port, true), HTTPServer::Protocol::HTTP },
        // {SocketAddress(FLAGS_ip, FLAGS_spdy_port, true), HTTPServer::Protocol::SPDY},
        // {SocketAddress(FLAGS_ip, FLAGS_h2_port, true), HTTPServer::Protocol::HTTP2},
    };

    if (FLAGS_threads <= 0) {
        FLAGS_threads = sysconf(_SC_NPROCESSORS_ONLN);
        CHECK(FLAGS_threads > 0);
    }

    wangle::SSLContextConfig sslCfg;
    sslCfg.isDefault = true;
    sslCfg.setCertificate(FLAGS_cert, FLAGS_key, "");
    IPs[0].sslConfigs.push_back(std::move(sslCfg));


    HTTPServerOptions options;
    options.threads = static_cast<size_t>(FLAGS_threads);
    options.idleTimeout = 60s;
    options.shutdownOn = { SIGINT, SIGTERM };
    options.enableContentCompression = false;
    options.handlerFactories =
        proxygen::RequestHandlerChain()
                .addThen<slackapi::SlackAPIHandlerFactory>().build();

    HTTPServer server(std::move(options));

    server.bind(IPs);

    // Start the db thread
    std::thread dbt([] { slackapi::slacktracks_db::getInstance().start(); });

    // Start the client thread
    std::thread client([] { slackapi::ClientRequestThread::getInstance().start(); });

    // Start HTTPServer mainloop in a separate thread
    std::thread t([&]() { server.start(); });

    t.join();
    return 0;
}
