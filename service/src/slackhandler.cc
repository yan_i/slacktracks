

#include <chrono>
#include <sstream>
#include <iostream>

#include <cstdlib>

#include <folly/Memory.h>
#include <folly/Portability.h>
#include <folly/io/async/EventBaseManager.h>
#include <glog/logging.h>
#include <proxygen/httpserver/HTTPServer.h>
#include <proxygen/httpserver/RequestHandlerFactory.h>
#include <proxygen/httpserver/ResponseBuilder.h>
#include <unistd.h>

#include <json.hpp>

#include "slackhandler.hpp"
#include "timer.h"

namespace slackapi
{
using namespace proxygen;
using json = nlohmann::json;

inline void
SlackAPIHandler::setBodyToJson(ResponseBuilder &response, const json &obj) const
{
    response.header("Contest-Type", "application/json");
    response.body(folly::IOBuf::copyBuffer(obj.dump()));
}


void
SlackAPIHandler::onRequest(std::unique_ptr<HTTPMessage> msg) noexcept
{
#if 0
    if (msg->getPath() == "/api/messages") {
        LOG(INFO) << "Got an API request";
        auto headers = msg->getHeaders();
        headers.forEach([&](const std::string &header, const std::string &val) {
            LOG(INFO) << header << ": " << val;
        });
    }
#endif
}


void
SlackAPIHandler::onBody(std::unique_ptr<folly::IOBuf> body) noexcept 
{
    if (body_) {
        body_->prependChain(std::move(body));
    } else {
        body_ = std::move(body);
    }
}

void
SlackAPIHandler::handleMessageEvent(const json &obj) const
{
    auto event_ts = obj["event_ts"];

    LOG(INFO) << "Got message: '"  << "' at " << event_ts;
}


void
SlackAPIHandler::onEOM() noexcept 
{
    ResponseBuilder response{ downstream_ };
    response.status(200, "OK");

    if (!body_ || body_->computeChainDataLength() == 0) {
        response.sendWithEOM();
        return;
    }

    auto req = json::parse(body_->moveToFbString().c_str());
    auto type = req["type"];

    if (type == "url_verification") {
        LOG(INFO) << "Verifying ourselves.";
        json payload { { "challenge", req["challenge"] } };
        setBodyToJson(response, payload);

    } else if (type == "event_callback") {
        try {
            Timer _{"json processing took"};

            api_->receivedJson(req);
        } catch(std::domain_error e) {
            LOG(ERROR) << "domain error: " << e.what();
            throw;
        }
    } else {
    }

    response.sendWithEOM();
}

void
SlackAPIHandler::onUpgrade(UpgradeProtocol proto) noexcept 
{
}

void
SlackAPIHandler::requestComplete() noexcept 
{
    delete this;
}

void
SlackAPIHandler::onError(ProxygenError err) noexcept 
{
    delete this;
}


}
