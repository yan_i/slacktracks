
#include <chrono>
#include <sstream>
#include <glog/logging.h>

#include "utils.h"

namespace slackapi {

std::chrono::system_clock::time_point
parseTime(const std::string timestamp_s)
{
    std::stringstream ss(timestamp_s);
    std::time_t seconds;
    unsigned micros;
    char dot;

    ss >> seconds >> dot >> micros;

    auto tp = std::chrono::system_clock::from_time_t(seconds);
    return tp + std::chrono::microseconds(micros);
}

std::string
timestampToString(const std::chrono::system_clock::time_point &tp)
{
    using namespace std::chrono;

    auto since_epoch = tp.time_since_epoch();

    auto s = duration_cast<seconds>(since_epoch);
    auto us = duration_cast<microseconds>(since_epoch);

    // if (s.count() == 0) {
    //     LOG(ERROR) << "Message with a 0 timestamp; weird.";
    //     return "";
    // }

    std::stringstream ss;
    auto micros = us.count() % 1000000;
    ss << s.count();
    if (micros)
        ss << "." << micros;
    return ss.str();
}

}

