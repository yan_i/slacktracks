#include <folly/io/async/EventBaseManager.h>
#include <proxygen/httpserver/RequestHandlerFactory.h>

#include "slackapi.hpp"

#pragma once

namespace slackapi {

using namespace proxygen;


class OAuthHandler : public RequestHandler
{
public:
    // TODO(yan): check that evb/api are not null
    explicit OAuthHandler(folly::EventBase *evb, slack *api) : evb_{evb}, api_{api}
    {

    }

    void onRequest(std::unique_ptr<HTTPMessage> msg) noexcept override;

    void onBody(std::unique_ptr<folly::IOBuf> body) noexcept override;

    void onEOM() noexcept override;

    void onUpgrade(UpgradeProtocol proto) noexcept override;

    void requestComplete() noexcept override {
        delete this;
    }

    void onError(ProxygenError err) noexcept override {
        delete this;
    }
private:
    std::unique_ptr<folly::IOBuf> body_;
    slack *api_;
    folly::EventBase *evb_;
    std::string code_;
};

}
