#include <iostream>
#include <sstream>

#include <gtest/gtest.h>
#include <json.hpp>

#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/filter/counter.hpp>

#include "../emojicodes.h"

namespace io = boost::iostreams;

namespace {
std::string filter(const std::string &str)
{
    auto emojis = " [                             \
                      { \"short_name\" : \"foo\", \
                        \"unified\" : \"61-62\"   \
                      },                          \
                      { \"short_name\" : \"bar\", \
                        \"unified\" : \"63\"      \
                      },                          \
                      { \"short_name\" : \"+1\",  \
                        \"unified\" : \"1F44D\"   \
                      }                           \
                    ] "_json; 

    std::stringstream ss;
    boost::iostreams::filtering_ostream out(
            emoji_shortcode_filter{emojis} | boost::ref(ss));
    out << str;
    out.flush();

    return ss.str();
}

}

TEST(emojicodes, unchanged)
{
    std::string from = "no emoji codes";
    auto returned = filter(from);
    ASSERT_EQ(from, returned);
}

TEST(emojicodes, single_colon)
{
    std::string from = "no emoji : codes";
    auto returned = filter(from);
    ASSERT_EQ(from, returned);
}

TEST(emojicodes, basic_emoji)
{
    std::string from = "blah :foo: ";
    auto returned = filter(from);
    std::string expected = "blah ab ";
    ASSERT_EQ(returned, expected);
}

TEST(emojicodes, aborts_correctly)
{
    std::string from = "blah :foa:";
    auto returned = filter(from);
    ASSERT_EQ(from, returned);
}

TEST(emojicodes, handles_ending_colon)
{
    std::string from = "asdf:";
    auto returned = filter(from);
    ASSERT_EQ(from, returned);
}

TEST(emojicodes, handles_sole_incorrect_shortcode)
{
    std::string from = ":asdf:";
    auto returned = filter(from);
    ASSERT_EQ(from, returned);
}

TEST(emojicodes, handles_consecutive_emojis)
{
    std::string from = ":foo::bar:";
    std::string expecting = "abc";
    auto returned = filter(from);
    ASSERT_EQ(expecting, returned);
}

TEST(emojicodes, handles_embedded_emojis)
{
    std::string from = ":asd:foo:f:";
    std::string expecting = ":asdabf:";
    auto returned = filter(from);
    ASSERT_EQ(expecting, returned);
}

TEST(emojicodes, correctly_replaces_single_emoji)
{
    std::string from = ":+1:";
    auto returned = filter(from);
    std::string expecting = "👍";
    ASSERT_EQ(expecting, returned);
}

TEST(emojicodes, long_emoji)
{
    std::string from = ":somethingelseentirelylalalalalalalalalalalalalalalalalalalalalala:";
    auto returned = filter(from);
    ASSERT_EQ(from, returned);
}

TEST(emojicodes, partial_emoji)
{
    std::string from = ":+1foo:";
    auto returned = filter(from);
    ASSERT_EQ(from, returned);
}

TEST(emojicodes, large_string)
{
    auto emojis = " [                             \
                      { \"short_name\" : \"foo\", \
                        \"unified\" : \"61-62\"   \
                      },                          \
                      { \"short_name\" : \"bar\", \
                        \"unified\" : \"63\"      \
                      },                          \
                      { \"short_name\" : \"+1\",  \
                        \"unified\" : \"1F44D\"   \
                      }                           \
                    ] "_json; 

    std::stringstream ss;
    boost::iostreams::filtering_ostream out(
        emoji_shortcode_filter{emojis} | boost::ref(ss));

    size_t kIterations = 1'000'000;
    for (int i = 0; i < kIterations; i++) {
        out << ":foo::bar::+1:";
    }
    out.flush();
    ASSERT_EQ(ss.tellp(), kIterations * 7);
}
