#include <sstream>
#include <fstream>

#include <folly/SocketAddress.h>
#include <proxygen/httpserver/RequestHandlerFactory.h>
#include <proxygen/httpserver/ResponseBuilder.h>
#include <boost/algorithm/string.hpp>
#include <boost/iostreams/filtering_stream.hpp>

#include "emojicodes.h"
#include "oauthhandler.h"
#include "client.h"
#include "clientthread.h"
#include "slackapi.hpp"
#include "slackevent.hpp"
#include "types.h"
#include "db.h"

namespace slackapi {

using namespace proxygen;


void OAuthHandler::onRequest(std::unique_ptr<HTTPMessage> msg) noexcept {

    if (!msg->hasQueryParam("code")) {
        return;
    }

    code_ = msg->getQueryParam("code");
}

void OAuthHandler::onBody(std::unique_ptr<folly::IOBuf> body) noexcept {
    if (body_) {
        body_->prependChain(std::move(body));
    } else {
        body_ = std::move(body);
    }
}

namespace {
std::string
renderChannels(const std::vector<channel> &channels)
{
    static emoji_shortcode_filter filter;
    std::stringstream ss;

    boost::iostreams::filtering_ostream out{boost::ref(filter) | boost::ref(ss)};

    ss << "<html><head><title>st</title></head><body><table>";
    ss << "<tr style=\"font-weight: bold\"><td>channel</td><td>about</td><td>members</td></tr>";

    for (auto &channel : channels) {
        auto topic = channel.topic;

        ss  << "<tr><td>" << channel.name << "</td>";

        out << "<td>" << topic << "</td>";
        out.flush();

        ss << "<td>" << channel.nmembers << "</td></tr>\n";
    }
    ss << "</table></body></html>";

    return ss.str();
}


}


void OAuthHandler::onEOM() noexcept {

    ResponseBuilder response{ downstream_ };
    response.status(200, "OK");

    // TODO(yan): move this into a single place

    //
    // Once we received the auth code, fetch a list of channels and find
    // a channel named 'music'
    //
    
    auto f = api_->authenticateTeam(code_)
        .then([this](team t) {
            // Add a new team; okay if we have already added it
            // XXX(yan): Recover correctly if that's the case
            auto &db = slacktracks_db::getInstance();

            db.addTeam(t);

            auto channels_f = api_->listChannels(t)
                .then([this, t, &db](std::vector<channel> channels) {
                    const auto music = std::find_if(std::cbegin(channels), std::cend(channels),
                                                    [](auto c) { return c.name == "music"; });

                    if (music != std::cend(channels)) {
                        db.addChannel(t, *music);
                        auto foo = api_->getChannelHistory(t, music->id, 9999);
                        foo.then([&](std::vector<Message> response) {
                                LOG(INFO) << "Received " << response.size() << " messages";
                                for (auto m : response) {
                                    db.storeMessage(std::make_shared<Message>(std::move(m)));
                                }
                                });
                    }

                    return std::move(channels);
            });
        return channels_f;
    });
    
    f.wait();


    if (f.hasValue()) {
        auto channels = f.get();

        auto bodyText = renderChannels(channels);

        response.body(bodyText);
    }
    response.sendWithEOM();


}

void OAuthHandler::onUpgrade(UpgradeProtocol proto) noexcept {

}


}
