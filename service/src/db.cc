#include <map>
#include <sstream>
#include "db.h"
#include "timer.h"

#include <pqxx/pqxx>

namespace slackapi {

using namespace events;

slacktracks_db::slacktracks_db()
{
}

// TODO(yan): very basic team cache, update when given a chance
std::map<std::string, std::shared_ptr<team>> team_cache_;

folly::Future<std::shared_ptr<team>>
slacktracks_db::checkTeam(std::string teamid)
{
    folly::Promise<std::shared_ptr<team>> p;
    auto f = p.getFuture();

    evb_.runInEventBaseThread([this, p=std::move(p), id=std::move(teamid)]() mutable{
            auto cached = team_cache_.find(id);
            if (cached != team_cache_.end()) {
                p.setValue(cached->second);
            } else {
                pqxx::work txn{conn_};

                auto query = "SELECT FROM teams WHERE id =" + txn.quote(id);
                auto result = txn.exec(query);
                if (result.size() != 1) {
                    p.setException(std::runtime_error{"don't have a team"});   
                }

                auto &row = result[0];
                // m.emplace(std::piecewise_construct,
                //           std::forward_as_tuple(1),
                //             std::forward_as_tuple(2.3, "hello"));

                auto ptr = std::make_shared<team>(
                            row[result.column_number("id")].as<std::string>(),
                            row[result.column_number("name")].as<std::string>(),
                            row[result.column_number("teamuser")].as<std::string>(),
                            row[result.column_number("scope")].as<std::string>(),
                            row[result.column_number("access_token")].as<std::string>());

                team_cache_[id] = ptr;

                p.setValue(ptr);
            }
    });

    return f;
}

folly::Future<bool>
slacktracks_db::addTeam(const team &team)
{
    folly::Promise<bool> promise;
    auto f = promise.getFuture();
    evb_.runInEventBaseThread([p=std::move(promise), team, this]() mutable{
            pqxx::work txn{conn_};

            auto query = "INSERT INTO teams(id, name, teamuser, scope, access_token) " 
                         "VALUES (" +
                                 txn.quote(team.id) + ", " +
                                 txn.quote(team.name) + ", " +
                                 txn.quote(team.user) + ", " +
                                 txn.quote(team.scope) + ", " +
                                 txn.quote(team.access_token) + ");";
            Timer t{query};

            try {
                txn.exec(query);
                txn.commit();

                p.setValue(true);
            } catch (pqxx::unique_violation &ex) {
                LOG(ERROR) << "Tried inserting team; but it already existed. ";
                p.setException(ex);
            } catch (pqxx::failure &ex) {
                p.setException(ex);
            }
            });
    return f;
}

folly::Future<folly::Unit>
slacktracks_db::addChannel(const team &team, const channel &channel)
{
    folly::Promise<folly::Unit> promise;
    auto f = promise.getFuture();
    evb_.runInEventBaseThread([p=std::move(promise), team, channel, this]() mutable{
            pqxx::work txn{conn_};
            std::stringstream ss;

            ss << "INSERT INTO channels(teamid, id, name, topic, ts) VALUES (" 
               << txn.quote(team.id) << ", " 
               << txn.quote(channel.id) << ", " 
               << txn.quote(channel.name) << ", " 
               << txn.quote(channel.topic) << ", "
               << "to_timestamp(" << channel.created << ") AT TIME ZONE 'UTC');";
            auto query = ss.str();
            Timer t{query};

            try {
                txn.exec(query);
                txn.commit();

                LOG(INFO) << "Added channel";

                p.setValue();
            } catch (pqxx::unique_violation &ex) {
                LOG(ERROR) << "Tried inserting team; but it already existed. ";
                p.setException(ex);
            } catch (pqxx::failure &ex) {
                p.setException(ex);
            }
            });
    return f;
}

folly::Future<folly::Unit>
slacktracks_db::storeMessage(std::shared_ptr<Message> msg)
{
    using std::move;

    folly::Promise<folly::Unit> promise;

    auto f = promise.getFuture();

    evb_.runInEventBaseThread([p = move(promise), m = move(msg), this]
        () mutable {
            pqxx::work txn{conn_};

            auto query = "INSERT INTO messages(ts, contents, slack_user) VALUES ("
                                 "TO_TIMESTAMP(" + txn.quote(m->timestamp_str()) + "), " +
                         txn.quote(m->text()) + ", " + txn.quote(m->user()) + ")";
            try {
                txn.exec(query);
                txn.commit();

                p.setValue();
            } catch (pqxx::unique_violation &ex) {
                LOG(ERROR) << "Tried inserting message; but it already existed. ";
                p.setException(ex);
            } catch (pqxx::failure &ex) {
                p.setException(ex);
            }
        });

    return f;
}
}
