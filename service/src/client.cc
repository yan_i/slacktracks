//
// Created by user on 11/24/16.
//

#include <wangle/ssl/SSLContextConfig.h>
#include <proxygen/lib/http/session/HTTPUpstreamSession.h>
#include <glog/logging.h>
#include <list>

#include "client.h"

namespace slackapi {
using namespace std::literals::string_literals;

HttpClient::~HttpClient()
{

}

std::string HttpClient::get(const std::string &url) {
    return ""s;
}

void HttpClient::initSsl(const std::string& certPath,
             const std::string& nextProtos) {
    ssl_context_ = std::make_shared<folly::SSLContext>();
    ssl_context_->setOptions(SSL_OP_NO_COMPRESSION);
    wangle::SSLContextConfig config;
    ssl_context_->ciphers(config.sslCiphers);
    ssl_context_->loadTrustedCertificates(certPath.c_str());
    std::list<std::string> nextProtoList;
    folly::splitTo<std::string>(',', nextProtos, std::inserter(nextProtoList,
                                                          nextProtoList.begin()));
    ssl_context_->setAdvertisedNextProtocols(nextProtoList);
}


void HttpClient::sslHandshakeFollowup(proxygen::HTTPUpstreamSession* session) noexcept {
    auto transport = session->getTransport();
    folly::AsyncSSLSocket* sslSocket =
        dynamic_cast<folly::AsyncSSLSocket*>(transport);

    const unsigned char* nextProto = nullptr;
    unsigned nextProtoLength = 0;
    sslSocket->getSelectedNextProtocol(&nextProto, &nextProtoLength);
    //   if (nextProto) {
    //       VLOG(1) << "Client selected next protocol " <<
    //               std::string((const char*)nextProto, nextProtoLength);
    //   } else {
    //       VLOG(1) << "Client did not select a next protocol";
    //   }

    // Note: This ssl session can be used by defining a member and setting
    // something like sslSession_ = sslSocket->getSSLSession() and then
    // passing it to the connector::connectSSL() method
}

void HttpClient::connectSuccess(proxygen::HTTPUpstreamSession* session) {
    VLOG(2) << "Connected to host";

    if (url_.isSecure()) {
        sslHandshakeFollowup(session);
    }

    auto txn_ = session->newTransaction(this);

    proxygen::HTTPMessage request_;
    request_.setMethod(proxygen::HTTPMethod::GET);
    request_.setHTTPVersion(1, 1);
    request_.setURL(url_.makeRelativeURL());
    request_.setSecure(url_.isSecure());
    request_.getHeaders().add("Accept", "*/*");
    request_.getHeaders().add(proxygen::HTTP_HEADER_HOST, url_.getHostAndPort());

    txn_->sendHeaders(request_);

    // unique_ptr<IOBuf> buf;
    // if (httpMethod_ == HTTPMethod::POST) {

    //     const uint16_t kReadSize = 4096;
    //     ifstream inputFile(inputFilename_, ios::in | ios::binary);

    //     // Reading from the file by chunks
    //     // Important note: It's pretty bad to call a blocking i/o function like
    //     // ifstream::read() in an eventloop - but for the sake of this simple
    //     // example, we'll do it.
    //     // An alternative would be to put this into some folly::AsyncReader
    //     // object.
    //     while (inputFile.good()) {
    //         buf = IOBuf::createCombined(kReadSize);
    //         inputFile.read((char*)buf->writableData(), kReadSize);
    //         buf->append(inputFile.gcount());
    //         txn_->sendBody(move(buf));
    //     }
    // }

    // note that sendBody() is called only for POST. It's fine not to call it
    // at all.

    txn_->sendEOM();

    session->closeWhenIdle();
}

void HttpClient::connectError(const folly::AsyncSocketException& ex) {
    LOG(ERROR) << "Failed connecting ";
}


}
