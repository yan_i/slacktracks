
#include <folly/io/async/EventBase.h>
#include <folly/io/async/EventBaseManager.h>
#include <folly/futures/Future.h>

#pragma once

namespace slackapi {

class ClientRequestThread {
public:

    static ClientRequestThread &getInstance() {
        static ClientRequestThread instance;
        return instance;
    }

    template <typename F>
    static void run(F &&f) {
        auto &crt = getInstance();
        crt.evb_.runInEventBaseThread(std::forward<F>(f));
    }

    ClientRequestThread()
    : timer_{folly::HHWheelTimer::newTimer(
              &evb_,
              std::chrono::milliseconds(folly::HHWheelTimer::DEFAULT_TICK_INTERVAL),  
              folly::AsyncTimeout::InternalEnum::NORMAL,                              
              std::chrono::seconds(5))}
    {
    }

    void start(void) {
        evb_.loopForever();
    }

    friend folly::Future<std::string> httpRequestSimple(std::string url_s);

private:
    folly::EventBase evb_;
    folly::HHWheelTimer::UniquePtr timer_;
};

folly::Future<std::string> httpRequestSimple(std::string url_s);

}
