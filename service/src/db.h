
#include <folly/futures/Future.h>
#include <folly/io/async/EventBase.h>
#include <pqxx/pqxx>

#include "types.h"
#include "slackevent.hpp"

#pragma once

namespace slackapi {

using namespace events;

class slacktracks_db {

public:
    static slacktracks_db &getInstance(void) {
        static slacktracks_db instance;
        return instance;
    }

    void start(void) {
        evb_.loopForever();
    }

    folly::Future<std::shared_ptr<team>> checkTeam(std::string teamid);

    folly::Future<bool> addTeam(const team &team);

    folly::Future<folly::Unit> addChannel(const team &team, const channel &channel);

    folly::Future<folly::Unit> storeMessage(std::shared_ptr<Message> msg);

private:
    slacktracks_db();
    ~slacktracks_db() { }

    folly::EventBase evb_;
    pqxx::connection conn_;
};

}
