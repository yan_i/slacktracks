--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: channels; Type: TABLE; Schema: public; Owner: yan
--

CREATE TABLE channels (
    id character(10) NOT NULL,
    teamid character(10),
    name text,
    topic text,
    ts timestamp with time zone
);


ALTER TABLE channels OWNER TO yan;

--
-- Name: messages; Type: TABLE; Schema: public; Owner: yan
--

CREATE TABLE messages (
    ts timestamp with time zone NOT NULL,
    contents text,
    slack_user character(10),
    channel character(10) REFERENCES channels (id)
);


ALTER TABLE messages OWNER TO yan;

--
-- Name: teams; Type: TABLE; Schema: public; Owner: yan
--

CREATE TABLE teams (
    id character(10) NOT NULL,
    name character varying,
    teamuser character(10),
    scope text,
    access_token text
);


ALTER TABLE teams OWNER TO yan;

--
-- Name: channels_pkey; Type: CONSTRAINT; Schema: public; Owner: yan
--

ALTER TABLE ONLY channels
    ADD CONSTRAINT channels_pkey PRIMARY KEY (id);


--
-- Name: messages_pkey; Type: CONSTRAINT; Schema: public; Owner: yan
--

ALTER TABLE ONLY messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (ts);


--
-- Name: teams_pkey; Type: CONSTRAINT; Schema: public; Owner: yan
--

ALTER TABLE ONLY teams
    ADD CONSTRAINT teams_pkey PRIMARY KEY (id);


--
-- Name: channels_teamid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: yan
--

ALTER TABLE ONLY channels
    ADD CONSTRAINT channels_teamid_fkey FOREIGN KEY (teamid) REFERENCES teams(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: channels; Type: ACL; Schema: public; Owner: yan
--

REVOKE ALL ON TABLE channels FROM PUBLIC;
REVOKE ALL ON TABLE channels FROM yan;
GRANT ALL ON TABLE channels TO yan;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE channels TO slacktracks;


--
-- Name: messages; Type: ACL; Schema: public; Owner: yan
--

REVOKE ALL ON TABLE messages FROM PUBLIC;
REVOKE ALL ON TABLE messages FROM yan;
GRANT ALL ON TABLE messages TO yan;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE messages TO slacktracks;


--
-- Name: teams; Type: ACL; Schema: public; Owner: yan
--

REVOKE ALL ON TABLE teams FROM PUBLIC;
REVOKE ALL ON TABLE teams FROM yan;
GRANT ALL ON TABLE teams TO yan;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE teams TO slacktracks;


--
-- PostgreSQL database dump complete
--


